module Main
  ( main,
  )
where

import Control.Monad (void)
import Control.Monad.IO.Class (liftIO)
import Core.Model.Cansi (Color (..), Slice (..))
import qualified Core.Model.Cansi as Cansi
import qualified Core.Model.MainList as MainList
import Core.ViewModel.Events (booleanDecision, handleSpecialKey)
import Data.IORef (modifyIORef', newIORef, readIORef)
import Data.List
import Data.List.Split (splitOn)
import Data.Semigroup (diff)
import Debug.Trace (trace)
import GHC.IORef (IORef)
import Test.QuickCheck
import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck
import Test.Tasty.Runners (TreeFold (TreeFold))

main = do
  -- bla <- generate treeGenerator
  -- bla <- generate statusLineGenerator
  -- bla <- generate statusGenerator
  -- print bla
  defaultMain $ testGroup "Tests" [events, mainList]

treeGenerator :: Gen Tree
treeGenerator = do
  lines <- listOf1 treeLineGenerator
  let res =
        Tree
          lines
          (foldl (\acc screenLine -> acc ++ [slices screenLine]) [] lines)
          (foldl (\acc screenLine -> acc ++ [codedLine screenLine]) [] lines)
  return res

data Tree = Tree
  { screenLines :: [ScreenLine],
    listOfSlices :: [[Slice]],
    codedLines :: [String]
  }

instance Show Tree where
  show (Tree screenLines _ _) =
    foldl
      (\acc line -> acc ++ show line ++ "\n")
      []
      screenLines

data ScreenLine = ScreenLine
  { slices :: [Slice],
    codedLine :: String
  }

instance Show ScreenLine where
  show (ScreenLine slices codedLine) =
    foldr
      (\slice acc -> text slice ++ acc)
      []
      slices

treeLineGenerator :: Gen ScreenLine
treeLineGenerator = do
  words <- wordsGenerator
  let plainStrings = map (++ " ") words
  lineHead <- elements ['\9679', '\9474']
  words <-
    if lineHead == '\9679'
      then do
        hash <- vectorOf 8 $ elements "0123456789abcdef"
        return ((lineHead : (" " ++ hash ++ " - ")) : plainStrings)
      else do
        return [lineHead : " "]
  screenLineGenerator words

wordsGenerator = do
  amount <- chooseInt (1, 5)
  vectorOf amount $ listOf1 $ elements ['a' .. 'z']

screenLineGenerator plainStrings = do
  colorCodes <- vectorOf (length plainStrings) $ elements ([30 .. 37] ++ [40 .. 47])
  return
    ScreenLine
      { slices = zipWith (\plainString colorCode -> Slice plainString (Color $ convertColorCode colorCode)) plainStrings colorCodes,
        codedLine = foldr (\(plainString, colorCode) acc -> "\x1b[" ++ show colorCode ++ "m" ++ plainString ++ "\x1b[0m" ++ acc) "" (zip plainStrings colorCodes)
      }
  where
    convertColorCode colorCode =
      let lastDigit = colorCode `mod` 10
       in case lastDigit of
            0 -> "black"
            1 -> "red"
            2 -> "green"
            3 -> "yellow"
            4 -> "blue"
            5 -> "magenta"
            6 -> "cyan"
            7 -> "white"

-- statusLineGenerator :: Gen ScreenLine
-- statusLineGenerator = do
--   words <- wordsGenerator
--   let randomPath = foldl (\acc word -> acc ++ word ++ "/") [] (init words) ++ (last words ++ ".hs")
--   screenLineGenerator [randomPath]
--
-- statusGenerator :: Gen Status
-- statusGenerator = do
--   amount <- chooseInt (1, 5)
--   paths <- vectorOf amount statusLineGenerator
--   unstagedLine <- screenLineGenerator ["Unstaged(" ++ show (length paths) ++ ")"]
--   return Status {statusLines = unstagedLine : paths}
--
-- data Status where
--   Status :: {statusLines :: [ScreenLine]} -> Status
--
-- instance Show Status where
--   show (Status statusLines) =
--     foldl
--       (\acc line -> acc ++ show line ++ "\n")
--       []
--       statusLines

mainList =
  testGroup
    "MainList.hs"
    [ testProperty
        "mainList_getUnstagedSlices"
        ( \paths ->
            MainList.getUnstagedSlices paths
              == statusSlices "Unstaged" "yellow" paths
        ),
      testProperty
        "mainList_getStagedSlices"
        ( \paths ->
            MainList.getStagedSlices paths
              == statusSlices "Staged" "green" paths
        ),
      test
        "mainList_getTreeSlices"
        ( \tree ->
            MainList.getTreeSlices (codedLines tree)
              == listOfSlices tree
        )
    ]
  where
    test name prop = testProperty name $ withMaxSuccess 100 $ forAll treeGenerator prop
    statusSlices title color items =
      [Slice (title ++ "(" ++ show (length items) ++ ")") (Color color)]
        : map (\item -> [Slice ("  + " ++ item) (Color color)]) items

events =
  testGroup
    "Events.hs"
    [ testCase "handleSpecialKey_CommitOpen" $
        assertEqual
          ""
          42
          (handleSpecialKey 999 42 7),
      testCase "handleSpecialKey_CommitClosed" $
        assertEqual
          ""
          7
          (handleSpecialKey 0 42 7),
      testCase "booleanDecision_True" $
        assertEqual
          ""
          1
          (booleanDecision True 1 0),
      testCase "booleanDecision_False" $
        assertEqual
          ""
          0
          (booleanDecision False 1 0)
    ]
