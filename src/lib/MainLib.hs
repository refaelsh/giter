module MainLib (main) where

import Brick
import Brick.BChan
import Control.Monad
import qualified Core.MainView (draw)
import Graphics.Vty (defaultConfig)
import Graphics.Vty.Attributes.Color
import Graphics.Vty.CrossPlatform
import Shell.AllEventsDispatcher (CustomEvent, MainState, WidgetName)
import qualified Shell.AllEventsDispatcher

main :: IO ()
main = do
  let buildVty = mkVty defaultConfig
  initialVty <- buildVty

  channel <- newBChan 100

  void $
    customMain
      initialVty
      buildVty
      (Just channel)
      (theApp channel)
      Shell.AllEventsDispatcher.createInitialState

theApp :: BChan CustomEvent -> App (MainState WidgetName) CustomEvent WidgetName
theApp channel =
  App
    { appDraw = Core.MainView.draw,
      appChooseCursor = showFirstCursor,
      appHandleEvent = Shell.AllEventsDispatcher.appHandleEvent,
      appStartEvent = Shell.AllEventsDispatcher.appStartEvent channel,
      appAttrMap = const theMap
    }

theMap :: AttrMap
theMap =
  attrMap
    (white `on` black)
    [ (attrName "red", red `on` black),
      (attrName "bright_red", brightRed `on` black),
      (attrName "green", green `on` black),
      (attrName "bright_green", brightGreen `on` black),
      (attrName "yellow", yellow `on` black),
      (attrName "bright_yellow", brightYellow `on` black),
      (attrName "blue", blue `on` black),
      (attrName "bright_blue", brightBlue `on` black),
      (attrName "magenta", magenta `on` black),
      (attrName "bright_magenta", brightMagenta `on` black),
      (attrName "cyan", cyan `on` black),
      (attrName "bright_cyan", brightCyan `on` black),
      (attrName "white", white `on` black),
      (attrName "bright_white", brightWhite `on` black),
      (attrName "red_selected", red `on` draculaBg),
      (attrName "bright_red_selected", brightRed `on` draculaBg),
      (attrName "green_selected", green `on` draculaBg),
      (attrName "bright_green_selected", brightGreen `on` draculaBg),
      (attrName "yellow_selected", yellow `on` draculaBg),
      (attrName "bright_yellow_selected", brightYellow `on` draculaBg),
      (attrName "blue_selected", blue `on` draculaBg),
      (attrName "bright_blue_selected", brightBlue `on` draculaBg),
      (attrName "magenta_selected", magenta `on` draculaBg),
      (attrName "bright_magenta_selected", brightMagenta `on` draculaBg),
      (attrName "cyan_selected", cyan `on` draculaBg),
      (attrName "bright_cyan_selected", brightCyan `on` draculaBg),
      (attrName "white_selected", white `on` draculaBg),
      (attrName "bright_white_selected", brightWhite `on` draculaBg)
    ]
  where
    draculaBg = rgbColor (68 :: Int) 71 90
