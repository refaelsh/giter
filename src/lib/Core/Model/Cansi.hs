module Core.Model.Cansi
  ( Slice (..),
    Color (..),
    categorize,
  )
where

import Data.Char (isAlpha)
import Data.List (findIndex)
import Data.List.Split
import Data.Maybe (fromJust)

data Slice = Slice
  { text :: String,
    color :: Color
  }
  deriving (Eq)

newtype Color = Color
  { string :: String
  }
  deriving (Eq)

categorize :: String -> [Slice]
categorize codedText =
  map
    (\pair -> Slice (snd pair) (colorFromCode (fst pair)))
    (textCodePairs codedText)
  where
    textCodePairs _codedText =
      filter
        (not . null . snd)
        ( map
            ( \str ->
                if '\x1b' `elem` str
                  then splitAt (fromJust (findIndex isAlpha str) + 1) str
                  else ("", str)
            )
            (deliminatedByEscape _codedText)
        )
    deliminatedByEscape = split (keepDelimsL $ oneOf ['\x1b'])

colorFromCode :: String -> Color
colorFromCode "" = Color "white"
colorFromCode code = case drop 2 (init code) of
  "0" -> Color "white"
  "30" -> Color "black"
  "31" -> Color "red"
  "32" -> Color "green"
  "33" -> Color "yellow"
  "34" -> Color "blue"
  "35" -> Color "magenta"
  "36" -> Color "cyan"
  "37" -> Color "white"
  "1;30" -> Color "bright_black"
  "1;31" -> Color "bright_red"
  "1;32" -> Color "bright_green"
  "1;33" -> Color "bright_yellow"
  "1;34" -> Color "bright_blue"
  "1;35" -> Color "bright_magenta"
  "1;36" -> Color "bright_cyan"
  "1;37" -> Color "bright_white"
  -- From here: https://en.wikipedia.org/wiki/ANSI_escape_code.
  -- 39 Default foreground color Implementation defined (according to standard)
  "39" -> Color "white"
  "40" -> Color "black"
  "41" -> Color "red"
  "42" -> Color "green"
  "43" -> Color "yellow"
  "44" -> Color "blue"
  "45" -> Color "magenta"
  "46" -> Color "cyan"
  "47" -> Color "white"
  "90" -> Color "bright_black"
  "91" -> Color "bright_red"
  "92" -> Color "bright_green"
  "93" -> Color "bright_yellow"
  "94" -> Color "bright_blue"
  "95" -> Color "bright_magenta"
  "96" -> Color "bright_cyan"
  "97" -> Color "bright_white"
  "100" -> Color "bright_black"
  "101" -> Color "bright_red"
  "102" -> Color "bright_green"
  "103" -> Color "bright_yellow"
  "104" -> Color "bright_blue"
  "105" -> Color "bright_magenta"
  "106" -> Color "bright_cyan"
  "107" -> Color "bright_white"
  _ -> Color "white"
