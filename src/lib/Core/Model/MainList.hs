module Core.Model.MainList
  ( getUnstagedSlices,
    getStagedSlices,
    getTreeSlices,
    constructLine,
    handleOpenClose,
    getPathFromListCurrentElement,
  )
where

import Core.Model.Cansi (Color (..), Slice (..))
import qualified Core.Model.Cansi as Cansi
import Data.List (isPrefixOf, (\\))

getUnstagedSlices :: [String] -> [[Slice]]
getUnstagedSlices = statusItems "yellow" "Unstaged"

getStagedSlices :: [String] -> [[Slice]]
getStagedSlices = statusItems "green" "Staged"

statusItems :: String -> [Char] -> [[Char]] -> [[Slice]]
statusItems _color title items =
  let titlePair =
        Slice
          (title ++ "(" ++ show (length items) ++ ")")
          (Color _color)
   in [titlePair]
        : map
          ( \item ->
              [ Slice
                  (openIndicator ++ item)
                  (Color _color)
              ]
          )
          items

getTreeSlices :: [String] -> [[Slice]]
getTreeSlices codedGitLogLines = beautify $ map Cansi.categorize codedGitLogLines
  where
    beautify = map (map (\slice -> Slice (replace (text slice)) (color slice)))
    replace datum =
      let repl '|' = '\9474'
          repl '/' = '\9585'
          repl '\\' = '\9586'
          repl '*' = '\9679'
          repl c = c
       in map repl datum

constructLine :: [Slice] -> Bool -> (Slice -> a) -> [a]
constructLine slices isSelected sliceColorConverter =
  let selectedColorSuffix = lineBgColor isSelected
   in map
        ( \slice ->
            sliceColorConverter
              ( Slice
                  (text slice)
                  (Color (string (color slice) ++ selectedColorSuffix))
              )
        )
        slices
  where
    lineBgColor _isSelected
      | _isSelected = "_selected"
      | otherwise = ""

handleOpenClose :: [[Slice]] -> Int -> [String] -> [[Slice]]
handleOpenClose list elementIndex diffCodedLines
  | openIndicator `isPrefixOf` text elementSlice =
      insert (modifiedList changeToClosedIndicator) (elementIndex + 1) diffSlices
  | closeIndicator `isPrefixOf` text elementSlice =
      modifiedList changeToOpenedIndicator \\ diffSlices
  | otherwise = list
  where
    elementSlice = head $ list !! elementIndex
    insert items index newItems = take index items ++ newItems ++ drop index items
    modifiedList modifier =
      modify
        list
        elementIndex
        [ Slice
            (modifier (text elementSlice))
            (color elementSlice)
        ]
    modify items index value = take index items ++ value : drop (index + 1) items
    changeToClosedIndicator datum = closeIndicator ++ drop (length openIndicator) datum
    changeToOpenedIndicator datum = openIndicator ++ drop (length closeIndicator) datum
    diffSlices = map (\diffCodedLine -> Slice "      " (Color "") : Cansi.categorize diffCodedLine) diffCodedLines

openIndicator :: String
openIndicator = "  + "

closeIndicator :: String
closeIndicator = "  - "

getPathFromListCurrentElement :: [Slice] -> [Char]
getPathFromListCurrentElement elementSlice =
  removeOpenCloseIndicator (text $ head elementSlice)
  where
    removeOpenCloseIndicator = drop (length openIndicator)
