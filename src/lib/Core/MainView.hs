module Core.MainView
  ( draw,
  )
where

import Brick (Widget, attrName, str, vBox, vLimit, withAttr)
import Brick.Widgets.Border (border, borderWithLabel)
import Brick.Widgets.Core (hBox)
import Brick.Widgets.Edit (getEditContents, renderEditor)
import Brick.Widgets.List (renderList)
import Core.Model.Cansi (Color (..), Slice (..))
import qualified Core.ViewModel.MainVM as MainVM
import Shell.AllEventsDispatcher (MainState (bricksList, commitMessageEditor, commitMessageHeight))

draw :: (Ord n, Show n) => MainState n -> [Widget n]
draw state =
  [ borderWithLabel (str "Giter") $
      vBox
        [ vLimit
            (commitMessageHeight state)
            (drawCommitMessageWidget (commitMessageEditor state)),
          renderTheList
            (bricksList state)
        ]
  ]
  where
    drawCommitMessageWidget editor =
      border $
        vBox
          [ hBox
              [ str "Enter commit message: ",
                renderEditor (str . unlines) True editor
              ],
            str " ",
            str "    After pressing Enter the following Git command will be executed:",
            str $ "    " ++ "git commit -m \"" ++ head (getEditContents editor) ++ "\" && git push"
          ]
    renderTheList =
      renderList
        ( \isSelected slices ->
            hBox $
              MainVM.constructLine
                slices
                isSelected
                ( \slice ->
                    withAttr
                      (attrName (string (color slice)))
                      (str (text slice))
                )
        )
        True
