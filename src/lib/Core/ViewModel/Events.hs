module Core.ViewModel.Events
  ( handleSpecialKey,
    booleanDecision,
  )
where

handleSpecialKey :: Int -> a -> a -> a
handleSpecialKey
  commitMessageHeight
  whenCommitDialogVisibleFunction
  otherwiseFunction
    | commitMessageHeight == 999 =
        whenCommitDialogVisibleFunction
    | otherwise =
        otherwiseFunction

booleanDecision :: Bool -> a -> a -> a
booleanDecision
  boolValue
  trueFunction
  falseFunction
    | boolValue =
        trueFunction
    | otherwise =
        falseFunction
