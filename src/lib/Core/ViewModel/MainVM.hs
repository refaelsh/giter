module Core.ViewModel.MainVM
  ( ColoredList,
    clear,
    appendUnstaged,
    appendStaged,
    appendTree,
    setSelectedLine,
    getSelectedLine,
    constructLine,
    handleOpenClose,
    getPathFromListCurrentElement,
  )
where

import Brick.Widgets.List (List, listClear, listElements, listReplace, listSelected, listSelectedElement)
import Core.Model.Cansi as Cansi
import qualified Core.Model.MainList as MainList
import Data.Maybe (fromMaybe)
import Data.Vector (fromList, toList)

-- TODO: All the functions here require unit testing.

type ColoredList n = List n [Slice]

clear :: ColoredList n -> ColoredList n
clear = listClear

appendUnstaged :: ColoredList n -> [String] -> ColoredList n
appendUnstaged list datum = append list (MainList.getUnstagedSlices datum)

appendStaged :: ColoredList n -> [String] -> ColoredList n
appendStaged list datum = append list (MainList.getStagedSlices datum)

appendTree :: ColoredList n -> [String] -> ColoredList n
appendTree list datum = append list (MainList.getTreeSlices datum)

append :: ColoredList n -> [[Slice]] -> ColoredList n
append list datum = listReplace (listElements list <> fromList datum) (Just 0) list

setSelectedLine :: ColoredList n -> Int -> ColoredList n
setSelectedLine list lineNumber = listReplace (listElements list) (Just lineNumber) list

getSelectedLine :: ColoredList n -> Int
getSelectedLine list = fromMaybe 0 (listSelected list)

constructLine :: [Slice] -> Bool -> (Slice -> a) -> [a]
constructLine = MainList.constructLine

handleOpenClose :: ColoredList n -> [String] -> ColoredList n
handleOpenClose list codedDiffLines =
  listReplace
    ( fromList
        ( MainList.handleOpenClose
            (toList (listElements list))
            (fromMaybe 0 $ listSelected list)
            codedDiffLines
        )
    )
    (listSelected list)
    list

getPathFromListCurrentElement :: ColoredList n -> String
getPathFromListCurrentElement list =
  MainList.getPathFromListCurrentElement
    ( snd
        ( fromMaybe
            (0, [Slice "" (Color "")])
            (listSelectedElement list)
        )
    )
