module Shell.AllEventsDispatcher
  ( MainState (commitMessageHeight, commitMessageEditor, bricksList),
    CustomEvent,
    WidgetName,
    createInitialState,
    appStartEvent,
    appHandleEvent,
  )
where

import Brick (BrickEvent (AppEvent, VtyEvent), EventM, get, modify, nestEventM, put)
import Brick.BChan (BChan, writeBChan)
import Brick.Widgets.Edit (Editor, editor, getEditContents, handleEditorEvent)
import Brick.Widgets.List (handleListEvent, handleListEventVi, list)
import Control.Concurrent (forkIO)
import Control.Monad.State (liftIO)
import Core.Model.Cansi (Color (Color), Slice (..))
import qualified Core.ViewModel.Events as Events
import Core.ViewModel.MainVM (ColoredList)
import qualified Core.ViewModel.MainVM as MainVM
import Data.Vector (fromList)
import Graphics.Vty.Input.Events (Event (EvKey), Key (KChar, KEnter, KEsc))
import qualified Shell.FileSystemWatcher
import qualified Shell.Sheller
import qualified Shell.Sheller as Sheller
import qualified Shell.StagedListShell as StagedListShell
import qualified Shell.UnstagedListShell as UnstagedListShell
import System.Directory (doesDirectoryExist, getCurrentDirectory)
import System.Exit (die)

data WidgetName = TreeWidgetName | CommitMessageName
  deriving (Show, Eq, Ord)

data CustomEvent = GitFSRefreshEvent

data MainState n = MainState
  { commitMessageHeight :: Int, -- TODO: Change this to bool instead of int.
    commitMessageEditor :: Editor String n,
    bricksList :: ColoredList n,
    selectedIndex :: Int
    -- TODO: Add here the following fields:
    -- stagedPathThatAreOpened :: [Path]
    -- unstagedPathThatAreOpened :: [Path]
  }

createInitialState :: MainState WidgetName
createInitialState =
  MainState
    { commitMessageHeight = 0,
      commitMessageEditor = editor CommitMessageName (Just 1) "",
      bricksList = list TreeWidgetName (fromList [[Slice "" (Color "")]]) 1,
      selectedIndex = 0
    }

appStartEvent :: BChan CustomEvent -> EventM WidgetName (MainState WidgetName) ()
appStartEvent channel = do
  let trigger = writeBChan channel GitFSRefreshEvent
  liftIO $ startWatchingFileSystem trigger
  liftIO trigger
  refreshEverything
  where
    startWatchingFileSystem :: IO () -> IO ()
    startWatchingFileSystem trigger = do
      currentFolder <- getCurrentDirectory
      isItGitRepo <- doesDirectoryExist (currentFolder ++ "/.git")
      Events.booleanDecision
        isItGitRepo
        (return ())
        (die "Could not find the .git folder here. Not a Git repo.")
      Shell.FileSystemWatcher.watch currentFolder trigger

refreshEverything :: EventM n (MainState n) ()
refreshEverything = do
  modify
    ( \state ->
        state
          { selectedIndex = MainVM.getSelectedLine (bricksList state)
          }
    )

  modifyBricksList (MainVM.clear . bricksList)

  let gitCommandPrefix = "git --no-pager -c color.ui=always --no-optional-locks "

  unstagedItems <- Shell.Sheller.execute (gitCommandPrefix ++ "diff --name-only && git ls-files --other --exclude-standard")
  modifyBricksList (\state -> MainVM.appendUnstaged (bricksList state) unstagedItems)

  stagedItems <- Shell.Sheller.execute (gitCommandPrefix ++ "diff --staged --name-only")
  modifyBricksList (\state -> MainVM.appendStaged (bricksList state) stagedItems)

  let gitLogThingy = "log --date-order --color-moved --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset)%C(auto)%d%C(reset)%n''          %C(white)%s%C(reset) %C(dim white)- %an%C(reset)' --all"
  codedGitLogLines <- Shell.Sheller.execute (gitCommandPrefix ++ gitLogThingy)
  modifyBricksList (\state -> MainVM.appendTree (bricksList state) codedGitLogLines)

  modifyBricksList (\state -> MainVM.setSelectedLine (bricksList state) (selectedIndex state))
  where
    modifyBricksList :: (MainState n -> ColoredList n) -> EventM n (MainState n) ()
    modifyBricksList modifyingFunction = modify (\state -> state {bricksList = modifyingFunction state})

appHandleEvent :: BrickEvent WidgetName CustomEvent -> EventM WidgetName (MainState WidgetName) ()
appHandleEvent (AppEvent GitFSRefreshEvent) = do
  refreshEverything
appHandleEvent (VtyEvent (EvKey (KChar 'o') [])) = do
  state <- get
  Events.handleSpecialKey
    (commitMessageHeight state)
    (handleGeneralKeyEvent (EvKey (KChar 'o') []) (commitMessageHeight state))
    ( do
        codedDiffLines <-
          Sheller.execute
            ( "git --no-pager -c color.ui=always --no-optional-locks diff HEAD --color "
                ++ MainVM.getPathFromListCurrentElement (bricksList state)
            )
        let newBricksList = MainVM.handleOpenClose (bricksList state) codedDiffLines
        put (state {bricksList = newBricksList})
    )
appHandleEvent (VtyEvent (EvKey (KChar 'c') [])) = do
  state <- get
  Events.handleSpecialKey
    (commitMessageHeight state)
    (handleGeneralKeyEvent (EvKey (KChar 'c') []) (commitMessageHeight state))
    (put (state {commitMessageHeight = 999}))
appHandleEvent (VtyEvent (EvKey KEsc [])) = do
  modify (\state -> state {commitMessageHeight = 0})
appHandleEvent (VtyEvent (EvKey KEnter [])) = do
  state <- get
  Events.handleSpecialKey
    (commitMessageHeight state)
    ( do
        let _editor = commitMessageEditor state
        _ <- liftIO $ forkIO $ do
          _ <- Sheller.execute ("git commit -m \"" ++ head (getEditContents _editor) ++ "\" && git push")
          return ()
        put (state {commitMessageHeight = 0})
    )
    (handleGeneralKeyEvent (EvKey KEnter []) (commitMessageHeight state))
appHandleEvent (VtyEvent e) = do
  state <- get
  handleGeneralKeyEvent e (commitMessageHeight state)
appHandleEvent _ = return ()

handleGeneralKeyEvent :: (Num a, Ord n, Eq a) => Event -> a -> EventM n (MainState n) ()
handleGeneralKeyEvent e 0 = do
  state <- get
  (newBricksList, _) <- nestEventM (bricksList state) $ do
    UnstagedListShell.handleEvent e
    StagedListShell.handleEvent e
    handleListEventVi handleListEvent e
  put (state {bricksList = newBricksList})
handleGeneralKeyEvent e 999 = do
  state <- get
  (newCommitMessageEditor, _) <- nestEventM (commitMessageEditor state) $ do
    handleEditorEvent (VtyEvent e)
  put (state {commitMessageEditor = newCommitMessageEditor})
handleGeneralKeyEvent _ _ = return ()
