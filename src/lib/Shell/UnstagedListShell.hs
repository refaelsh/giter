module Shell.UnstagedListShell
  ( handleEvent,
  )
where

import Brick (get)
import Brick.Types (EventM)
import Core.ViewModel.MainVM (ColoredList)
import qualified Core.ViewModel.MainVM as MainVM
import Graphics.Vty (Event (EvKey), Key (KChar))
import qualified Shell.Sheller as Sheller

handleEvent :: Ord n => Event -> EventM n (ColoredList n) ()
handleEvent e = do
  list <- get
  case e of
    (EvKey (KChar 'S') []) -> do
      _ <- Sheller.execute "git --no-pager -c color.ui=always --no-optional-locks add ."
      return ()
    (EvKey (KChar 'r') []) -> do
      _ <- Sheller.execute $ "git --no-pager -c color.ui=always --no-optional-locks restore " ++ MainVM.getPathFromListCurrentElement list
      return ()
    (EvKey (KChar 'R') []) -> do
      _ <- Sheller.execute "git --no-pager -c color.ui=always --no-optional-locks restore ."
      return ()
    _ -> return ()
