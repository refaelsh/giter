module Shell.StagedListShell
  ( handleEvent,
  )
where

import Brick (EventM)
import Core.ViewModel.MainVM (ColoredList)
import Graphics.Vty (Event (EvKey), Key (KChar))
import qualified Shell.Sheller as Sheller

handleEvent :: (Ord n) => Event -> EventM n (ColoredList n) ()
handleEvent e = do
  case e of
    (EvKey (KChar 'U') []) -> do
      _ <- Sheller.execute "git --no-pager -c color.ui=always --no-optional-locks reset"
      return ()
    _ -> return ()
