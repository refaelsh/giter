module Shell.Sheller
  ( execute,
  )
where

import Control.Monad.State (MonadIO (liftIO))
import Data.Tuple.Select (sel2, sel3)
import System.Process (readCreateProcessWithExitCode, shell)

execute :: (MonadIO m) => String -> m [String]
execute command = liftIO $ do
  bbb <- readCreateProcessWithExitCode (shell command) ""
  return $ lines $ sel2 bbb ++ sel3 bbb
