module Shell.FileSystemWatcher
  ( watch,
  )
where

import Control.Concurrent (threadDelay)
import Control.Monad (forever, void)
import GHC.Conc (forkIO)
import System.FSNotify (watchTree, withManager)

-- TODO: This is just one function. It should not be its own module.

watch :: FilePath -> IO () -> IO ()
watch path trigger = do
  void $ forkIO $ withManager $ \mgr -> do
    void $
      watchTree
        mgr
        path
        (const True)
        (\_ -> do trigger)
    forever $ threadDelay maxBound
