nnoremap <F5> :wa <bar> :set makeprg=cabal\ build <bar> :compiler ghc <bar> :make <CR>
nnoremap <F7> :wa <bar> :set makeprg=cabal\ test <bar> :compiler ghc <bar> :make <CR>
nnoremap <F10> :silent !kitty -e cabal run& <CR> :redraw<CR>        
