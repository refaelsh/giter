### Giter - the philosophy

This project is about yet another UI client for Git, but different.
It will mix elements from both CLI and GUI/TUI.
It will have as minimalistic features set as possible, because, well, I don't like `feature-bloat`.
For the more advanced Git stuff, that are rarely used, one can always use the terminal as usual.
I've seen many Git client wrappers over the years, and, IMHO, they all suffer from one big problem: they are all too complex.
Git is complex enough, and it takes time and effort to learn it.
If you add on top of it learning the quirks of some wrapper, it becomes too much.
The UI should be a simple wrapper around Git CLI. For example, I've once seen a GUI that had a `pull` button, but what it was doing in the background 
was something like `git pull --all --force --modules --init` and etc., and the user had no clue.
It forced the user to learn the quirks of that specific wrapper and not Git in general.
Giter, would not have such a button, in fact, Giter would almost have no buttons at all.
Every action on the UI, will reflect to the user what Git command was used in the background.
The main inspiration for the UI is the great [Magit](https://magit.vc/) plugin for Emacs.

### Manual

'S' - stage all.

'U' - unstage all.

'r' - restore (as in `git restore /path/to/file`) specific file.

'R' - restore all files.

'c' - open the commit dialog.

### Screenshot

![screenshot](screenshot.png "Screenshot")

### Cross Platformability

Works on Linux only. PRs for winblows support are welcome.

### Contributions guide

Of course contributions are always welcome. Please feel free to open a PR and/or open an issue.
